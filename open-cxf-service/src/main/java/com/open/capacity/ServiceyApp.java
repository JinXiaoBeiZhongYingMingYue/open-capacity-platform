package com.open.capacity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceyApp {

	public static void main(String[] args) {

		 SpringApplication.run(ServiceyApp.class, args);

	 
	}
}

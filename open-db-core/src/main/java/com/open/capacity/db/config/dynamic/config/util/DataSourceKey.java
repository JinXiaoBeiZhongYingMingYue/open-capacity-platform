package com.open.capacity.db.config.dynamic.config.util; 
/**
 * 数据源定义
 *
 * @author owen
 * @create 2017年7月2日
 */
public enum DataSourceKey {
    crm, bill
}